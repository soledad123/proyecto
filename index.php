<?php 

include 'model/conexion.php';
$sentencia=$bd->query("select * from estudiante;");
$estudiante=$sentencia->fetchAll(PDO::FETCH_OBJ);
	
?>


<!DOCTYPE html>
<html lang="es">
<head>
	<title>lista de estudiantes</title>
	<meta charset="utf-8">
     <link rel="stylesheet" href="estilos.css">
    </head>
    <body>
    <div id="main-container">
         <center>
             <h3 class="table_title">Lista de estudiantes</h3>
             <table>
             <thead>
                  <tr>
                  <td>N°</td>
                  <td>Nombre</td>
                  <td>Monto</td>
                  <td></td>
                  </tr> 
                  </thead> 
                  <?php 
                   foreach ($estudiante as $dato) {

                    ?>
					<tr>
						<td><?php echo $dato->N°; ?></td>
						<td><?php echo $dato->Nombre; ?></td>
						<td><?php echo $dato->Monto; ?></td>
                              <td><a href="editar.php?N°=<?php echo $dato->N°;?>">Editar</a></td>
			
			
					</tr>
					<?php
				}
			?>
                    
               </table>
               </center>
               </div> 
               <hr>
               <div class="container-add">
               <h2 class="container_title">Registrar estudiante:</h2>
               <form method="POST" action="insertar.php" class="container_form">
               <table>
               <tr>
                          <td class="container_label">N°: </td>
                          <td><input type="text" name="txtN°" class="container_input"></td>
                          </tr>
                     <tr>
                          <td class="container_label">Nombre: </td>
                          <td><input type="text" name="txtnombre" class="container_input"></td>
                          </tr>
                     <tr>
                          <td class="container_label">Monto: </td>
                          <td><input type="text" name="txtmonto" class="container_input"></td>
                          </tr>
                          <input type="hidden" name="oculto" value="1">
                          <tr>
                          
                          <td><input class="container_submit" type="submit" value="INGRESAR"></td>
                          </tr>
                </table>
                </form>
                </div>
    </body>
</html>
