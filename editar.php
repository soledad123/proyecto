<?php  
        if(!isset($_GET['N°'])){
			header('location: index.php');
		}
		include 'model/conexion.php';
		$N° = $_GET['N°'];
		$sentencia = $bd->prepare("SELECT * FROM estudiante WHERE N° = ?;");
		$sentencia->execute([$N°]);
		$persona = $sentencia->fetch(PDO::FETCH_OBJ);
		//print_r($persona);
	?>

<!DOCTYPE html>
<html>
<head>
	<title>Editar estudiante</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="estilos.css">
</head>
<body>
		<center>
		<h3>Editar estudiante:</h3>

		<form method="POST" action="editarProceso.php">
			<table>
				<tr>
					<td>Nombre: </td>
					<td><input size="100" type="text" name="Nombre" value="<?php echo $persona->Nombre; ?>"></td>
				</tr>
				<tr>
					<td>Monto: </td>
					<td><input size="100" type="text" name="Monto" value="<?php echo $persona->Monto; ?>"></td>
				</tr>
				<tr>
				    <input type="hidden" name="oculto">
					<input type="hidden" name="id2" value="<?php echo $persona->N°;?>">
					<td colspan="2"><input type="submit" value="ACTUALIZAR"></td>
				</tr>
			</table>
		</form>
	
		</center>
 
</body>
</html>